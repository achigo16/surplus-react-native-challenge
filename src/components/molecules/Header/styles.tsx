import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  abstractShape: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: height(168),
    backgroundColor: '#009788',
    borderBottomRightRadius: width(20),
    borderBottomLeftRadius: width(20),
  },
  container: {
    paddingHorizontal: width(20),
    paddingTop: width(15),
    paddingBottom: width(25),
  },
  topContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  topLeftContainer: {
    flex: 1,
  },
  locationLabelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: height(3),
  },
  locationTitle: {
    color: '#fff',
    marginRight: width(5),
  },
  locationIcon: {
    width: width(10),
    aspectRatio: 1,
    backgroundColor: '#fff',
    borderRadius: width(10),
  },
  locationText: {
    fontWeight: '700',
    color: '#fff',
  },
  cartIcon: {
    width: width(30),
    aspectRatio: 1,
    backgroundColor: '#fff',
    borderRadius: width(30),
  },
  greatingText: {
    marginTop: height(25),
    marginBottom: height(10),
    fontWeight: '700',
    color: '#fff',
  },
  searchContainer: {
    backgroundColor: '#fff',
    height: height(60),
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    borderRadius: width(8),
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: width(15),
  },
  search: {
    flex: 1,
  },
  searchText: {
    color: 'rgba(0,0,0,0.4)',
  },
  searchBtn: {
    width: width(25),
    aspectRatio: 1,
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderRadius: width(25),
  },
});
