import React from 'react';

import {View} from 'react-native';

import styles from './styles';
import {Text} from 'react-native-paper';

type props = {};

const Header = ({}: props) => {
  return (
    <>
      <View style={styles.abstractShape} />

      <View style={styles.container}>
        <View style={styles.topContainer}>
          <View style={styles.topLeftContainer}>
            <View style={styles.locationLabelContainer}>
              <Text variant="labelMedium" style={styles.locationTitle}>
                Lokasi Anda
              </Text>
              <View style={styles.locationIcon} />
            </View>

            <Text
              variant="bodyLarge"
              style={styles.locationText}
              numberOfLines={1}>
              My Location
            </Text>
          </View>

          <View style={styles.cartIcon} />
        </View>

        <Text variant="headlineSmall" style={styles.greatingText}>
          Hi, Surlus Hero!
        </Text>

        <View style={styles.searchContainer}>
          <View style={styles.search}>
            <Text variant="bodyMedium" style={styles.searchText}>
              Mau selamatkan makanan apa hari ini?
            </Text>
          </View>
          <View style={styles.searchBtn} />
        </View>
      </View>
    </>
  );
};

export default Header;
