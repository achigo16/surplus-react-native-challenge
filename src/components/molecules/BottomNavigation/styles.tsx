import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: height(10),
  },
  image: {
    width: width(30),
    aspectRatio: 1,
    backgroundColor: 'rgba(0,0,0,0.2)',
    marginBottom: height(5),
    borderRadius: width(30),
  },
  activeImage: {
    backgroundColor: '#009788',
  },
  text: {
    textTransform: 'uppercase',
    color: 'rgba(0,0,0,0.4)',
    fontWeight: '500',
    letterSpacing: 1,
  },
  activeText: {
    color: '#009788',
  },
});
