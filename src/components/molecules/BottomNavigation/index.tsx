import React from 'react';

import {View, TouchableOpacity} from 'react-native';
import {Text} from 'react-native-paper';

import styles from './styles';

type Props = {
  active?: number;
};

type Menu = {
  title: string;
};

const MENUS: Menu[] = [
  {
    title: 'Discover',
  },
  {
    title: 'Pesanan',
  },
  {
    title: 'Forum',
  },
  {
    title: 'Profil',
  },
];

const BottomNavigation = ({active}: Props) => {
  const renderButton = (data: Menu, index: number) => {
    return (
      <TouchableOpacity key={'btn-' + index} style={styles.button}>
        <View style={[styles.image, active === index && styles.activeImage]} />
        <Text
          variant="bodySmall"
          style={[styles.text, active === index && styles.activeText]}>
          {data.title}
        </Text>
      </TouchableOpacity>
    );
  };

  return <View style={styles.container}>{MENUS.map(renderButton)}</View>;
};

export default BottomNavigation;
