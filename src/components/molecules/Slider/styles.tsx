import {StyleSheet} from 'react-native';
import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';

export default StyleSheet.create({
  container: {
    height: height(260),
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    paddingHorizontal: width(20),
  },
  image: {
    flex: 1,
    marginBottom: height(36),
    resizeMode: 'stretch',
    borderRadius: width(10),
  },
  dot: {
    backgroundColor: 'rgba(0,0,0,.2)',
    width: width(8),
    aspectRatio: 1,
    borderRadius: width(4),
    margin: width(3),
  },
  activeDot: {
    backgroundColor: '#009788',
    width: width(8),
    aspectRatio: 1,
    borderRadius: width(4),
    margin: width(3),
  },
  pagination: {
    left: width(20),
    bottom: height(10),
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  skeleton: {
    height: height(260),
  },
});
