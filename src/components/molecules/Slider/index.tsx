import React from 'react';

import {View, Image} from 'react-native';

import Swiper from 'react-native-swiper';

import styles from './styles';

type props = {};

const Header = ({}: props) => {
  const [images, setImages] = React.useState<string[]>([]);

  React.useEffect(() => {
    const getImages = async () => {
      const url =
        'https://api.unsplash.com/photos/random?orientation=landscape&count=4&client_id=Tb9illCEV0WeslJADQ6H6YQZ_v2_0U3NYvxfKipmlQQ';

      fetch(url)
        .then(response => {
          return response.json();
        })
        .then(data => {
          const newImages = (data ?? []).reduce(
            (accumulative: string[], item: any) => {
              return [...accumulative, item.urls.regular];
            },
            [],
          );

          setImages(newImages);
        });
    };

    if (images.length === 0) {
      getImages();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderBannerImage = (data: string, index: number) => {
    return (
      <View style={styles.slide} key={'img-' + index}>
        <Image style={styles.image} source={{uri: data}} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {images.length > 0 ? (
        <Swiper
          containerStyle={styles.container}
          dot={<View style={styles.dot} />}
          activeDot={<View style={styles.activeDot} />}
          paginationStyle={styles.pagination}
          loop>
          {images.map(renderBannerImage)}
        </Swiper>
      ) : (
        <View style={styles.skeleton} />
      )}
    </View>
  );
};

export default Header;
