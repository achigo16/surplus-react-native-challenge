import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,151,0,0.3)',
  },
  topContainer: {
    height: height(230),
    paddingHorizontal: width(25),
    paddingVertical: height(25),
    justifyContent: 'flex-end',
  },
  title: {
    marginBottom: height(5),
    fontWeight: 'bold',
    color: '#fff',
  },
  subtitle: {
    color: '#fff',
    width: width(200),
  },
});
