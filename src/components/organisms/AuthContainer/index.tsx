import React from 'react';

import {View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Text} from 'react-native-paper';

import AuthCard from '@app/components/atoms/AuthCard';

import styles from './styles';

type props = {
  children: React.ReactNode;
  title?: string;
  subtitle?: string;
};

const AuthContainer = ({children, title, subtitle}: props) => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <View style={styles.container}>
        <View style={styles.topContainer}>
          {title && (
            <Text style={styles.title} variant="headlineLarge">
              {title}
            </Text>
          )}
          {subtitle && (
            <Text style={styles.subtitle} variant="bodyMedium">
              {subtitle}
            </Text>
          )}
        </View>

        <AuthCard>{children}</AuthCard>
      </View>
    </SafeAreaView>
  );
};

export default AuthContainer;
