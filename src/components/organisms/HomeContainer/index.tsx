import React from 'react';

import {View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import Navigation from '@app/components/molecules/BottomNavigation';

import styles from './styles';

type props = {
  children: React.ReactNode;
  activeMenu?: number;
};

const HomeContainer = ({children, activeMenu}: props) => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <View style={styles.container}>{children}</View>
      <Navigation active={activeMenu ?? 0} />
    </SafeAreaView>
  );
};

export default HomeContainer;
