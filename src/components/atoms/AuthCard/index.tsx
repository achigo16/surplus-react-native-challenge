import React from 'react';

import {Card} from 'react-native-paper';

import styles from './styles';

type props = {
  children?: React.ReactNode;
};

const AuthCard = ({children}: props) => {
  return (
    <Card style={styles.container} mode="contained">
      {children}
    </Card>
  );
};

export default AuthCard;
