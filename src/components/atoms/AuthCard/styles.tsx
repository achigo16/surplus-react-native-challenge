import {StyleSheet} from 'react-native';

import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 0,
    borderTopRightRadius: height(45),
    borderTopLeftRadius: height(45),
    overflow: 'hidden',
    paddingHorizontal: width(30),
    paddingTop: width(40),
    backgroundColor: '#FFF',
  },
});
