import React from 'react';

import {View} from 'react-native';
import {Text} from 'react-native-paper';

import styles from './styles';

type props = {
  label?: string;
};

const Seperator = ({label}: props) => {
  return (
    <View style={styles.container}>
      <View style={styles.line} />
      {label && (
        <Text style={styles.text} variant="bodyLarge">
          {label}
        </Text>
      )}
      <View style={styles.line} />
    </View>
  );
};

export default Seperator;
