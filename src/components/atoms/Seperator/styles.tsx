import {StyleSheet} from 'react-native';

import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: height(25),
    alignItems: 'center',
    height: 'auto',
  },
  line: {
    flex: 1,
    height: height(1),
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  text: {
    paddingHorizontal: width(20),
    color: 'rgba(0,0,0,0.2)',
  },
});
