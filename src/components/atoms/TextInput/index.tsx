import React from 'react';

import {View} from 'react-native';
import {Controller, Control, FieldValues, FieldErrors} from 'react-hook-form';
import {TextInput as BaseTextInput, Text} from 'react-native-paper';

import {TextInputProps} from '@app/types/textinput';
import styles from './styles';

type props = {
  control: Control<FieldValues, any>;
  data: TextInputProps;
  isLast?: boolean;
  error?: FieldErrors<FieldValues>;
  isSubmitted?: boolean;
};

const TextInput = ({control, data, isLast, error, isSubmitted}: props) => {
  const isError = error?.[data.name] !== undefined;
  const message = (error?.[data.name]?.message ?? '').toString();

  return (
    <Controller
      control={control}
      rules={data.rules}
      render={({field: {onChange, onBlur, value}}) => (
        // eslint-disable-next-line react-native/no-inline-styles
        <View style={[styles.container, isLast && {marginBottom: 0}]}>
          <Text
            style={[styles.label, isError && styles.labelError]}
            variant="bodyLarge">
            {data.label}
          </Text>
          <BaseTextInput
            placeholder={data.placeholder}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
            keyboardType={data.keyboardType}
            outlineStyle={[
              styles.textInputOutline,
              isSubmitted && styles.textInputOutlineSuccess,
              isError && styles.textInputOutlineError,
            ]}
            style={styles.textInput}
            mode="outlined"
            placeholderTextColor={'rgba(0,0,0,0.2)'}
            secureTextEntry={data.secureTextEntry}
          />
          {isError && (
            <Text style={styles.errorLabel} variant="bodySmall">
              {message}
            </Text>
          )}
        </View>
      )}
      name={data.name}
    />
  );
};

export default TextInput;
