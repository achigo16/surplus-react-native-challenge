import {StyleSheet} from 'react-native';

import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';

export default StyleSheet.create({
  container: {
    marginBottom: height(25),
  },
  textInputOutline: {
    borderColor: 'rgba(0,0,0,0.1)',
    borderWidth: 2,
    borderRadius: width(8),
  },
  textInput: {
    fontWeight: '200',
    backgroundColor: '#fff',
  },

  label: {
    fontWeight: '700',
  },

  labelError: {
    color: 'red',
  },
  textInputOutlineSuccess: {
    borderColor: '#009788',
  },
  textInputOutlineError: {
    borderColor: 'red',
  },
  errorLabel: {
    marginTop: height(4),
    color: 'red',
  },
});
