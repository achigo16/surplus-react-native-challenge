import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import SplashScreen from '@app/screens/Splash';
import AuthScreen from '@app/screens/Auth';
import LoginScreen from '@app/screens/Login';
import RegisterScreen from '@app/screens/Register';
import MainScreen from '@app/screens/Main';

import {RootStackParamList} from '@app/types/navigation';

const {Navigator, Screen} = createNativeStackNavigator<RootStackParamList>();

const Routes = () => {
  return (
    <Navigator initialRouteName="Splash" screenOptions={{headerShown: false}}>
      <Screen name="Splash" component={SplashScreen} />
      <Screen name="Auth" component={AuthScreen} />
      <Screen name="Login" component={LoginScreen} />
      <Screen name="Register" component={RegisterScreen} />
      <Screen name="Main" component={MainScreen} />
    </Navigator>
  );
};

export default Routes;
