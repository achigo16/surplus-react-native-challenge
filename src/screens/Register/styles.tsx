import {StyleSheet} from 'react-native';

import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';

export default StyleSheet.create({
  container: {flex: 1, backgroundColor: '#fff'},
  loginBtn: {
    marginVertical: height(20),
    paddingVertical: height(5),
    borderRadius: height(100),
  },
  loginBtnText: {
    fontWeight: '700',
    color: '#fff',
  },
  SSOBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  ssoBtn: {
    paddingVertical: height(5),
    borderRadius: height(100),
    backgroundColor: 'rgba(0,0,0,0.1)',
    marginHorizontal: width(8),
    flex: 0.5,
  },
  ssoBtnText: {
    fontWeight: '700',
    color: '#000',
  },
  RegisterBtnContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: height(35),
  },
  registerBtn: {
    textDecorationLine: 'underline',
    color: '#009788',
    fontWeight: '700',
  },
  termConditionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginTop: height(40),
  },
  termConditionText: {
    color: 'rgba(0,0,0,0.4)',
  },
  termConditionButton: {
    color: '#EBD688',
  },
});
