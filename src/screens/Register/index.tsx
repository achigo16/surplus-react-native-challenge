import React from 'react';

import {ScrollView, View, TouchableOpacity} from 'react-native';
import {useForm} from 'react-hook-form';
import {Button, Text} from 'react-native-paper';

import Container from '@app/components/organisms/AuthContainer';
import TextInput from '@app/components/atoms/TextInput';
import Seperator from '@app/components/atoms/Seperator';

import {DefaultNavigationProps} from '@app/types/navigation';
import {TextInputProps} from '@app/types/textinput';

import styles from './styles';

const Register = ({navigation}: DefaultNavigationProps<'Register'>) => {
  const {
    control,
    handleSubmit,
    formState: {errors, isSubmitted},
    watch,
  } = useForm();

  const INPUT: TextInputProps[] = [
    {
      placeholder: 'Alamat e-mail kamu',
      label: 'E-mail',
      keyboardType: 'email-address',
      name: 'email',
      rules: {
        required: {
          message: 'Email tidak boleh kosong',
          value: true,
        },
        pattern: {
          message: 'Format email salah',
          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
        },
      },
    },
    {
      placeholder: 'Masukan kata sandi',
      label: 'Kata sandi',
      name: 'password',
      secureTextEntry: true,
      rules: {
        required: {
          message: 'Kata sandi tidak boleh kosong',
          value: true,
        },
      },
    },
    {
      placeholder: 'Ulang kata sandi',
      label: 'Ulang kata sandi',
      name: 're-password',
      secureTextEntry: true,
      rules: {
        required: {
          message: 'Ulang kata sandi tidak boleh kosong',
          value: true,
        },
        validate: (val: string) => {
          if (watch('password') !== val) {
            return 'Password tidak sama';
          }
        },
      },
    },
  ];

  const handleLoginButtonPress = () => {
    navigation.navigate('Login');
  };

  const handleSubmitForm = (data: any) => console.log(data);

  const renderTextInput = (item: TextInputProps, index: number) => {
    return (
      <TextInput
        key={'item-' + index}
        control={control}
        data={item}
        error={errors}
        isSubmitted={isSubmitted}
      />
    );
  };

  return (
    <ScrollView style={styles.container}>
      <Container title="Daftar" subtitle="Lengkapi isian untu mendaftar">
        {INPUT.map(renderTextInput)}

        <Button
          mode="contained"
          style={styles.loginBtn}
          onPress={handleSubmit(handleSubmitForm)}>
          <Text variant="bodyLarge" style={styles.loginBtnText}>
            Daftar
          </Text>
        </Button>

        <Seperator label="Atau" />

        <View style={styles.SSOBtnContainer}>
          <Button mode="contained" style={styles.ssoBtn}>
            <Text variant="bodyLarge" style={styles.ssoBtnText}>
              Google
            </Text>
          </Button>
        </View>

        <View style={styles.termConditionContainer}>
          <Text style={styles.termConditionText} variant="bodySmall">
            Dengan daftar atau masuk, Anda menerima{' '}
          </Text>
          <TouchableOpacity>
            <Text style={styles.termConditionButton} variant="bodySmall">
              syarat dan kententuan
            </Text>
          </TouchableOpacity>
          <Text style={styles.termConditionText} variant="bodySmall">
            {' '}
            serta{' '}
          </Text>
          <TouchableOpacity>
            <Text style={styles.termConditionButton} variant="bodySmall">
              kebijakan privasi
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.RegisterBtnContainer}>
          <Text variant="bodyMedium">Sudah punya akun? </Text>
          <TouchableOpacity onPress={handleLoginButtonPress}>
            <Text variant="bodyMedium" style={styles.registerBtn}>
              Yuk masuk
            </Text>
          </TouchableOpacity>
        </View>
      </Container>
    </ScrollView>
  );
};

export default Register;
