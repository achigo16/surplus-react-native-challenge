import React from 'react';

import {ScrollView, View, TouchableOpacity} from 'react-native';
import {useForm} from 'react-hook-form';
import {Button, Text} from 'react-native-paper';

import Container from '@app/components/organisms/AuthContainer';
import TextInput from '@app/components/atoms/TextInput';
import Seperator from '@app/components/atoms/Seperator';

import {DefaultNavigationProps} from '@app/types/navigation';
import {TextInputProps} from '@app/types/textinput';

import styles from './styles';

const Login = ({navigation}: DefaultNavigationProps<'Login'>) => {
  const {
    control,
    handleSubmit,
    formState: {errors, isSubmitted},
    setError,
  } = useForm();

  const INPUT: TextInputProps[] = [
    {
      placeholder: 'Alamat e-mail kamu',
      label: 'E-mail',
      name: 'email',
      keyboardType: 'email-address',
      rules: {
        required: {
          message: 'Email tidak boleh kosong',
          value: true,
        },
        pattern: {
          message: 'Format email salah',
          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
        },
      },
    },
    {
      placeholder: 'Masukan kata sandi',
      label: 'Kata sandi',
      name: 'password',
      secureTextEntry: true,
      rules: {
        required: {
          message: 'Kata sandi tidak boleh kosong',
          value: true,
        },
      },
    },
  ];

  const handleRegisterButtonPress = () => {
    navigation.navigate('Register');
  };

  const handleSubmitForm = (data: any) => {
    const {email, password} = data;

    if (email !== 'test@test.com' || password !== '12345678') {
      setError('password', {
        type: 'custom',
        message: 'Email atau Password salah',
      });
      return;
    }

    navigation.navigate('Main');
  };

  const renderTextInput = (item: TextInputProps, index: number) => {
    return (
      <TextInput
        key={'item-' + index}
        control={control}
        data={item}
        isLast={INPUT.length === index + 1}
        error={errors}
        isSubmitted={isSubmitted}
      />
    );
  };

  return (
    <ScrollView style={styles.container}>
      <Container
        title="Masuk"
        subtitle="Pastikan kamu sudah pernah membuat akun Surplus">
        {INPUT.map(renderTextInput)}
        <Button
          mode="text"
          style={styles.forgotPasswordBtn}
          labelStyle={styles.forgotPasswordBtnText}>
          Lupa kata sandi?
        </Button>

        <Button
          mode="contained"
          style={styles.loginBtn}
          onPress={handleSubmit(handleSubmitForm)}>
          <Text variant="bodyLarge" style={styles.loginBtnText}>
            Masuk
          </Text>
        </Button>

        <Seperator label="Atau" />

        <View style={styles.SSOBtnContainer}>
          <Button mode="contained" style={styles.ssoBtn}>
            <Text variant="bodyLarge" style={styles.ssoBtnText}>
              Facebook
            </Text>
          </Button>
          <Button mode="contained" style={styles.ssoBtn}>
            <Text variant="bodyLarge" style={styles.ssoBtnText}>
              Google
            </Text>
          </Button>
        </View>

        <View style={styles.RegisterBtnContainer}>
          <Text variant="bodyMedium">Belum punya akun? </Text>
          <TouchableOpacity onPress={handleRegisterButtonPress}>
            <Text variant="bodyMedium" style={styles.registerBtn}>
              Yuk daftar
            </Text>
          </TouchableOpacity>
        </View>
      </Container>
    </ScrollView>
  );
};

export default Login;
