import React from 'react';

import {View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import {DefaultNavigationProps} from '@app/types/navigation';

import styles from './styles';

const Splash = ({navigation}: DefaultNavigationProps<'Splash'>) => {
  React.useEffect(() => {
    const gotToLogin = async () => {
      navigation.navigate('Auth');
    };

    setTimeout(() => {
      gotToLogin();
    }, 5000);
  }, [navigation]);

  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <View style={styles.container}>
        <View style={styles.image} />
      </View>
    </SafeAreaView>
  );
};

export default Splash;
