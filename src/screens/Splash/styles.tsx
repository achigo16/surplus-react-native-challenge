import {widthComparedByReference as width} from '@app/utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: width(180),
    aspectRatio: 1,
    backgroundColor: '#009788',
    borderRadius: width(90),
  },
});
