import React from 'react';

import {SafeAreaView} from 'react-native-safe-area-context';
import {Button} from 'react-native-paper';

import Container from '@app/components/organisms/HomeContainer';
import Header from '@app/components/molecules/Header';
import Slider from '@app/components/molecules/Slider';

import {DefaultNavigationProps} from '@app/types/navigation';
import styles from './styles';

const Main = ({}: DefaultNavigationProps<'Main'>) => {
  return (
    <SafeAreaView style={styles.safeAreaContainer}>
      <Container activeMenu={0}>
        <Header />

        <Slider />

        <Button
          style={styles.voucherBtn}
          labelStyle={styles.voucherBtnText}
          mode="contained">
          Mau lihat voucher kamu? Yuk daftar!
        </Button>
      </Container>
    </SafeAreaView>
  );
};

export default Main;
