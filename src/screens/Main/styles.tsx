import {StyleSheet} from 'react-native';

import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';

export default StyleSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  voucherBtn: {
    marginHorizontal: width(20),
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: width(10),
    paddingVertical: height(12),
  },
  voucherBtnText: {
    color: '#009788',
    fontSize: height(16),
    fontWeight: '700',
  },
});
