import {
  heightComparedByReference as height,
  widthComparedByReference as width,
} from '@app/utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,151,0,0.3)',
  },
  topContainer: {
    height: height(400),
    paddingHorizontal: width(30),
    paddingVertical: height(25),
    alignItems: 'flex-end',
  },
  headingContainer: {
    marginBottom: height(40),
  },
  title: {
    textAlign: 'center',
    marginBottom: height(10),
    fontWeight: 'bold',
  },
  subtitle: {
    textAlign: 'center',
    color: 'rgba(0,0,0,0.4)',
  },
  buttonContainer: {
    borderRadius: height(50),
    marginBottom: height(20),
  },
  button: {
    paddingVertical: height(6),
  },
  termConditionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: height(20),
  },
  termConditionText: {
    color: 'rgba(0,0,0,0.4)',
  },
  termConditionButton: {
    color: '#EBD688',
  },
});
