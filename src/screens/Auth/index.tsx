import React from 'react';

import {View, TouchableOpacity} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTheme, Button, Text} from 'react-native-paper';

import AuthCard from '@app/components/atoms/AuthCard';

import {DefaultNavigationProps} from '@app/types/navigation';

import styles from './styles';

const Auth = ({navigation}: DefaultNavigationProps<'Auth'>) => {
  const theme = useTheme();

  const handleSkipButtonPress = () => {
    navigation.navigate('Main');
  };

  const handleRegisterButtonPress = () => {
    navigation.navigate('Register');
  };

  const handleLoginButtonPress = () => {
    navigation.navigate('Login');
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.topContainer}>
        <Button
          onPress={handleSkipButtonPress}
          style={{borderColor: theme.colors.primary}}
          mode="outlined">
          Lewati
        </Button>
      </View>

      <AuthCard>
        <View style={styles.headingContainer}>
          <Text style={styles.title} variant="headlineSmall">
            Selamat datang di Surplus
          </Text>
          <Text style={styles.subtitle} variant="bodyMedium">
            Selamatkan makanan berlebih di aplikasi Surplus agar tidak terbuat
            sia-sia
          </Text>
        </View>
        <Button
          onPress={handleRegisterButtonPress}
          style={[styles.buttonContainer, {borderColor: theme.colors.primary}]}
          contentStyle={styles.button}
          mode="contained">
          Daftar
        </Button>
        <Button
          onPress={handleLoginButtonPress}
          style={[styles.buttonContainer, {borderColor: theme.colors.primary}]}
          contentStyle={styles.button}
          mode="outlined">
          Sudah punya akun? Masuk
        </Button>

        <View style={styles.termConditionContainer}>
          <Text style={styles.termConditionText} variant="bodySmall">
            Dengan daftar atau masuk, Anda menerima{' '}
          </Text>
          <TouchableOpacity>
            <Text style={styles.termConditionButton} variant="bodySmall">
              syarat dan kententuan
            </Text>
          </TouchableOpacity>
          <Text style={styles.termConditionText} variant="bodySmall">
            {' '}
            serta{' '}
          </Text>
          <TouchableOpacity>
            <Text style={styles.termConditionButton} variant="bodySmall">
              kebijakan privasi
            </Text>
          </TouchableOpacity>
        </View>
      </AuthCard>
    </SafeAreaView>
  );
};

export default Auth;
