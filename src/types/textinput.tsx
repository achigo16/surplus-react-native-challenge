import {FieldValues, RegisterOptions} from 'react-hook-form';
import {TextInputProps as BaseTextInputProps} from 'react-native-paper';

export type TextInputProps = BaseTextInputProps & {
  name: string;
  rules?:
    | Omit<
        RegisterOptions<FieldValues, string>,
        'valueAsNumber' | 'valueAsDate' | 'setValueAs' | 'disabled'
      >
    | undefined;
};
